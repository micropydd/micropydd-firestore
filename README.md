[![pipeline status](https://gitlab.com/micropydd/micropydd-firestore/badges/master/pipeline.svg)](https://gitlab.com/micropydd/micropydd-firestore/commits/master) [![coverage report](https://gitlab.com/micropydd/micropydd-firestore/badges/master/coverage.svg)](https://gitlab.com/micropydd/micropydd-firestore/commits/master) [![PyPI version](https://badge.fury.io/py/MicroPyDD-firestore.svg)](https://badge.fury.io/py/MicroPyDD-firestore)

# MicroPyDD Firestore

This module is a simple wrapper that simplifies the setup of firestore. 