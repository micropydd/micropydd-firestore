from micropydd_firestore import VERSION


class FirestoreConfig:
    MICROPYDD_FIRESTORE_VERSION: str = VERSION

    ROOT_COLLECTION = 'root_collection/collection_item'
