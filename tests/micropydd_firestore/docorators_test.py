from dataclasses import dataclass

from doublex import Mock

from micropydd_firestore.decorators import fs_to_object_type


@dataclass
class MockObject:
    some_string_property: str
    some_int_property: int


def test_result_to_type_element():
    # Given
    with Mock() as document_mock:
        document_mock.to_dict().returns({
            'some_string_property': 'string',
            'some_int_property': 1.
        })

    @fs_to_object_type(MockObject)
    def example_function():
        return document_mock

    # When
    result = example_function()

    # Then
    assert isinstance(result, MockObject)


def test_result_to_type_list():
    # Given
    with Mock() as document_mock:
        document_mock.to_dict().returns({
            'some_string_property': 'string',
            'some_int_property': 1.
        })

    with Mock() as documents_mock:
        documents_mock.stream().returns([
            document_mock,
            document_mock,
        ])

    @fs_to_object_type(MockObject, is_list=True)
    def example_function():
        return documents_mock.stream()

    # When
    result = example_function()

    # Then
    assert isinstance(result, list)
    assert isinstance(result[0], MockObject)
    assert isinstance(result[1], MockObject)
