from dataclasses import dataclass
import time

import micropydd
import pytest
from doublex import Mock
from google.cloud import firestore

import micropydd_firestore.firestore_entity as firestore_entity_module
from micropydd_firestore.firestore_entity import FirestoreEntityService
from micropydd_firestore.config import FirestoreConfig
from micropydd_firestore.models import Entity, PaginatedResult


@dataclass
class EntityExample(Entity):
    example: str


@pytest.fixture
def firestore_client():
    return firestore.Client()


@pytest.fixture
def root_collection():
    return f'root_collection/collection_item_{time.time()}'


def get_timestamp_mock():
    return 111


@pytest.fixture
def firestore_entity_service(firestore_client, root_collection, monkeypatch):
    monkeypatch.setattr(firestore_entity_module, 'get_timestamp', get_timestamp_mock)
    with Mock() as mock_config:
        mock_config.ROOT_COLLECTION = root_collection
    micropydd.app_context = {
        firestore.Client: firestore_client,
        FirestoreConfig: mock_config,
    }

    return FirestoreEntityService('entity_example', EntityExample, firestore.Client())


def test_create(firestore_entity_service, firestore_client, root_collection):
    # Given
    entity = EntityExample(example='SomeValue')

    # When
    firestore_entity_service.create('id', entity)

    # Then
    result = firestore_client.collection(f'{root_collection}/entity_example').document('id').get().to_dict()
    assert result == {
        'example': 'SomeValue',
        'created_at': get_timestamp_mock()
    }


def test_find_by_id(firestore_entity_service, firestore_client, root_collection):
    # Given
    firestore_client.collection(f'{root_collection}/entity_example').document('find_by_id').create({
        'example': 'Example Entity',
        'created_at': get_timestamp_mock()
    })

    # When
    result = firestore_entity_service.find_by_id('find_by_id')

    # Then
    assert isinstance(result, EntityExample)
    assert result.example == 'Example Entity'


def test_find_all(firestore_entity_service, firestore_client, root_collection):
    # Given
    firestore_client.collection(f'{root_collection}/entity_example').document('find_by_id').create({
        'example': 'Example Entity',
        'created_at': get_timestamp_mock()
    })

    # When
    result = firestore_entity_service.find_all()

    # Then
    assert isinstance(result, PaginatedResult)
    assert isinstance(result.results[0], EntityExample)
    assert result.results[0].example == 'Example Entity'


def test_find_with_where(firestore_entity_service, firestore_client, root_collection):
    # Given
    firestore_client.collection(f'{root_collection}/entity_example').document('find_by_id').create({
        'example': 'Example Entity',
        'created_at': get_timestamp_mock()
    })

    # When
    result = firestore_entity_service.find(where=[('example', '==', 'Example Entity')])

    # Then
    assert isinstance(result, PaginatedResult)
    assert isinstance(result.results[0], EntityExample)
    assert result.results[0].example == 'Example Entity'


def test_update(firestore_entity_service, firestore_client, root_collection):
    # Given
    firestore_client.collection(f'{root_collection}/entity_example').document('update').create({
        'example': 'Example Entity',
        'created_at': get_timestamp_mock()
    })

    # When
    firestore_entity_service.update('update', EntityExample(example='Some Other Value'))

    # Then
    result = firestore_client.collection(f'{root_collection}/entity_example').document('update').get().to_dict()
    assert result == {
        'example': 'Some Other Value',
        'created_at': get_timestamp_mock()
    }


def test_delete_by_id(firestore_entity_service, firestore_client, root_collection):
    # Given
    firestore_client.collection(f'{root_collection}/entity_example').document('delete').create({
        'example': 'Example Entity',
        'created_at': get_timestamp_mock()
    })

    # When
    firestore_entity_service.delete_by_id('delete')

    # Then

    res = firestore_client.collection(f'{root_collection}/entity_example').document('delete').get()
    assert res.to_dict() is None
