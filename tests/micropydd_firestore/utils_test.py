import micropydd
from doublex import Spy, Mock, assert_that, called
from google.cloud import firestore

from micropydd_firestore.config import FirestoreConfig
from micropydd_firestore.utils import root_collection


def test_root_collection():
    # Given
    with Mock() as mock_config:
        mock_config.ROOT_COLLECTION = 'main_collection'
    firestore_spy = Spy(firestore.Client)
    micropydd.app_context = {
        firestore.Client: firestore_spy,
        FirestoreConfig: mock_config,
    }

    # When
    root_collection('collection_child')

    # Then
    assert_that(firestore_spy.collection, called().with_args(f'main_collection/collection_child'))
